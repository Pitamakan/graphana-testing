import datetime
import requests
import json

numdays = 425
base = datetime.datetime.today()
date_list = [base - datetime.timedelta(days=x) for x in range(numdays)]
date_list.reverse()

data = []
print(numdays)
for i, date in enumerate(date_list):
    print(i, date)
    resp_usd = requests.get(f"https://api.exchangeratesapi.io/{date.strftime('%Y-%m-%d')}?symbols=RUB&base=USD")
    resp_eur = requests.get(f"https://api.exchangeratesapi.io/{date.strftime('%Y-%m-%d')}?symbols=RUB&base=EUR")
    usd_price = json.loads(resp_usd.content)["rates"]["RUB"]
    eur_price = json.loads(resp_eur.content)["rates"]["RUB"]
    
    data.append( {
        'time': date.strftime('%Y-%m-%d'),
        'measurement': 'price',
        'fields': {
           'USD': usd_price,
           'EUR': eur_price
        }
    })
print(data)    
