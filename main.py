from influxdb import InfluxDBClient

f = open("data.txt", "r")
data = []
for line in f:
    date, usd_price, eur_price = line.split()
    usd_price = float(usd_price)
    eur_price = float(eur_price)
    data.append( {
        'time': date+"T00:00:00Z",
        'measurement': 'price',
        'fields': {
           'USD': usd_price,
           'EUR': eur_price
        }
    })
#print(data)

client = InfluxDBClient(host='influxdb', port=8086, 
                       username='root', password='root', database='prices')
#client.create_database('prices')
client.write_points(data)
a = client.query('select * from prices;')
print(list(a))